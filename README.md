
I created a few classes using different datasets to cover ARIMA process for time series such as differecing, autocorrelation, regression analysis( to make thinkgs simple I used linear regression) and  moving average. 

Little explanation what is time series:
Time series statistics is a quite different animal from ordinary statistics (  because we take into consideration another factor or feature -  time). Time itself becomes as another independent variable ( or lags in time)  .  
 
Time series has 3 major components: Trend, Seasonality, Noise.
 
1. Trend - a component (linear on non-linear) that changes over time.
          2. Seasonality -  linear or (most often) nonlinear component that changes over time and doesn’t repeat.
          3. Noise – a non systematic component.
 
 
So far I  learned how to calculate  from the ground up  (manually) only the Differences for time series.  Moving Average  and   Autocorrelation (its calculation is related to Pearson correlation coefficient) I used ready made libraries. Moving average, Differences , Autocorrelation are the fundamentals of ARIMA model.
First step of  ARIMA model first is to make model stationary (probability distribution doesn’t change when shifted in time ) . The number of times it needs to be differenced is reflected in d parameter.
Then we calculate AR part of ARIMA model, from my understanding, MA component of ARIMA model is not used very often, because it is very unpredictive (but can be very useful for predicting recessions for example ).
 
In short I want to describe   components of ARIMA model :
Differencing: Why we need differencing ? 
Just small overview of differencing why we need them:  Differencing we use to remove trend from time series. Significant changes in level of trend requires lag 1 differencing.
Strong changes of slope usually require second order non seasonal (why non seasonal ? ) differencing. As far as I understood we stop on  second order differencing. Differencing doesn’t look to complicated 😊 . It is a D parameter in the model.
Moving average. Why we need it ? The moving average here tries to take into consideration  past error or random shock that cannot by calculated by the autoregressive component. It is a parameter Q
Autocorrelation. its calculation is related to Pearson correlation coefficient, but it calculate correlations to the observation with previous time steps, called lags. It is a P parameter in the model.
 
After calculating all above 3 coefficients we can easy predict the Cerebri next step( next value) in time series.
 
 
 
 
 
 
